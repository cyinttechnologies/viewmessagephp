<?php

namespace CYINT\ComponentsPHP\Classes;
class ViewMessage
{
    protected static $icons = [
        'success' => 'glyphicon glyphicon-success',
        'warning' => 'glyphicon glyphicon-warning',
        'danger' => 'glyphicon glyphicon-danger',
    ];

    public static function constructMessage($message = null, $type = null, $custom_icon = null)
    {
        return [
            'text' => $message,
            'type' => 'alert alert-' . $type,
            'icon' => empty($custom_icon) ? self::$icons[$type] : $custom_icon
        ];
    }

    public static function exceptionMessage($Ex, $dev=true)
    {
        if($dev)
            $message = $Ex->getMessage() . ' Line: ' . $Ex->getLine() . '<br />Stack Trace:' . $Ex->getTraceAsString();
        else
            $message = $Ex->getMessage();
    
        return self::constructMessage($message, 'danger');
    }

}

?>
